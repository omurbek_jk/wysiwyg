class Blog < ApplicationRecord
    
  validates :title, presence: true, length: {maximum: 140}
  #This validates presence of body
  validates :content, presence: true
    
end
